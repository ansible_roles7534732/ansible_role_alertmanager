Alertmanager ansible role
=========

Installs alertmanager(https://github.com/prometheus/alertmanager)

Requirements
------------

—

Role Variables
--------------

See defaults/main/*

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install alertmanager
  hosts: alertmanager
  become: true
  gather_facts: true
  roles:
    - alertmanager
      tags:
        - role_alertmanager
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
